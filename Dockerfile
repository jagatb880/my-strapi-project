
# FROM node:18-alpine

# WORKDIR /app

# COPY package.json .

# RUN npm install

# RUN npm run build

# CMD [ "npm" ,"run", "develop" ]

FROM node:18-alpine
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
ENV NODE_ENV development
RUN npm run build
CMD ["npm", "run", "develop"]
# # Use the official Node.js 14 image as base
# FROM node:18-alpine

# # Set the working directory inside the container
# WORKDIR /app

# # Copy package.json and package-lock.json to the working directory
# COPY package*.json ./

# # Install Strapi dependencies
# RUN npm install

# # Copy the Strapi application files to the working directory
# COPY . .

# # Set environment variables
# ENV NODE_ENV development

# # Build Strapi application
# RUN npm run build

# # Expose the port on which Strapi runs
# EXPOSE 1337

# # Run the Strapi application
# CMD ["npm", "run", "develop"]
# FROM node:18-alpine
# # Installing libvips-dev for sharp Compatibility
# RUN apk update && apk add --no-cache build-base gcc autoconf automake zlib-dev libpng-dev nasm bash vips-dev git
# ARG NODE_ENV=development
# ENV NODE_ENV=${NODE_ENV}

# WORKDIR /opt/
# COPY package.json yarn.lock ./
# RUN yarn global add node-gyp
# RUN yarn config set network-timeout 600000 -g && yarn install
# ENV PATH /opt/node_modules/.bin:$PATH

# WORKDIR /opt/app
# COPY . .
# RUN chown -R node:node /opt/app
# USER node
# RUN ["yarn", "build"]
# EXPOSE 1337
# CMD ["yarn", "develop"]
